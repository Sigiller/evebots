﻿var Measurement = Sanderling?.MemoryMeasurementParsed?.Value;

if (Measurement.WindowRegionalMarket == null) {
	Sanderling.KeyboardPressCombined(new[]{ VirtualKeyCode.LMENU, VirtualKeyCode.VK_R});
}

Measurement = Sanderling?.MemoryMeasurementParsed?.Value;
checkObject(Measurement.WindowRegionalMarket[0]);
var RegionalMarket = Measurement.WindowRegionalMarket[0];

if (RegionalMarket.MyOrders == null) {
	var MyOrdersLabel = RegionalMarket.LabelText.ElementAt(3);
	Sanderling.MouseClickLeft(MyOrdersLabel);
}

Measurement = Sanderling?.MemoryMeasurementParsed?.Value;
RegionalMarket = Measurement.WindowRegionalMarket[0];
checkObject(RegionalMarket.MyOrders.SellOrderView.Entry);
var SellingOrders = RegionalMarket.MyOrders.SellOrderView.Entry;

for (int i = 0; i < SellingOrders.Length; i++) {
	var Order = SellingOrders.ElementAt(i);
	ModifyPrices(Order);
}

void ModifyPrices(Sanderling.Interface.MemoryStruct.IUIElement Order) {
	Sanderling.MouseClickRight(Order);
	clickContextMenu(3);
	checkObject(Sanderling?.MemoryMeasurementParsed?.Value.WindowRegionalMarket[0].SelectedItemTypeDetails.MarketData.SellerView.Entry);
	var MarketData = Sanderling?.MemoryMeasurementParsed?.Value.WindowRegionalMarket[0].SelectedItemTypeDetails.MarketData.SellerView.Entry;
	List<Sanderling.Interface.MemoryStruct.IListEntry> MyOrders = new List<Sanderling.Interface.MemoryStruct.IListEntry>();
	if (MarketData == null) { return; }
	
	for(int j = 0; j < MarketData.Length; j++) {
		var elem = MarketData.ElementAt(j);
		int? colors = elem.ListBackgroundColor?.Length;
		if (colors > 0 && j != 0) { MyOrders.Add(elem); }
	}
	
	for(int k = 0; k < MyOrders.Count(); k++) {
		var _order = MyOrders.ElementAt(k);
		Sanderling.MouseClickRight(_order);
		Host.Delay(100);
		clickContextMenu(1);
		decimal minPrice = getMinimumSellerPrice(MarketData);
		Random rnd = new Random();
		decimal newPrice = Decimal.Subtract(minPrice, .01m + rnd.Next(0, 100));
		Host.Log(newPrice);
	}
}

void clickContextMenu(int item) {
	Sanderling.MouseClickRight(Sanderling?.MemoryMeasurementParsed?.Value.Menu.ElementAt(0).Entry.ElementAt(item));
}

decimal getMinimumSellerPrice(Sanderling.Interface.MemoryStruct.IListEntry[] MarketData) {
	var PriceStr = Array.Find(MarketData.ElementAt(0).ListColumnCellLabel, x => x.Key.LabelText.ElementAt(0).Text.Contains("Price")).Value;
	PriceStr = PriceStr.Substring(0, PriceStr.Length - 4).Replace(",", ".");
	var _price = PriceStr.Split(PriceStr[3]);
	PriceStr = String.Join("", _price);
	
	return decimal.Parse(PriceStr);
}

void checkObject(object target) {
	if (target == null) {
		Host.Delay(1000);	
		Host.Log("Wait for " + target);
	} 
}
